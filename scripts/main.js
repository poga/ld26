require.config({
    paths: {
        jaws: "libs/jaws",
        _: "libs/lodash.min",
        pubsub: "libs/pubsub"
    },

    shim: {
        jaws: {
            exports: "jaws"
        }
    }
});

require(['jaws', 'states/intro'], function (jaws, introState) {
    jaws.assets.add("images/hero.png");
    jaws.assets.add("images/flag.png");
    jaws.start(introState);
});