define(["jaws", "_", "pubsub", "states/intro/hero"], function (jaws, _, PubSub, Hero) {
    var introState = {
        sprites: null,
        score: 0,
        flag: null,
        gameStart: false,

        setup: function () {
            var state = this;
            this.sprites = [];
            this.score = 0;
            this.gameStart = false;

            var flag = new jaws.Sprite({image: "images/flag.png", x: 400, y: 292, scale: 1.5});
            flag.update = function () {
                if (jaws.pressed("left")) {
                    if (this.x >= 0) {
                        this.moveTo(this.x - 10, this.y);
                        PubSub.publish("flagMove", [this.x, this.y]);
                        if (state.gameStart === false) {
                            state.score = 0;
                        }
                        state.gameStart = true;
                    } else {
                        this.moveTo(0, this.y);
                    }
                }
                if (jaws.pressed("right")) {
                    if (this.rect().right <= jaws.width) {
                        this.moveTo(this.x + 10, this.y);
                        PubSub.publish("flagMove", [this.x, this.y]);
                        if (state.gameStart === false) {
                            state.score = 0;
                        }
                        state.gameStart = true;
                    } else {
                        this.moveTo(jaws.width - this.width, this.y);
                    }
                }
            };
            this.sprites.push(flag);
            this.flag = flag;

            var hero = new Hero({x_vel: 5, y_vel: -5});
            this.sprites.push(hero);

            PubSub.subscribe("score", function (msg, data) {
                state.score += 1;
            });
        },

        update: function () {
            _.each(this.sprites, function (sp) {
                if (sp.update) { sp.update(); }
            });

            this.sprites = _.reject(this.sprites, function (sp) {
                if (sp.x > jaws.width || sp.y > jaws.height) {
                    return true;
                } else {
                    return false;
                }
            });

            var prob = Math.random();
            if (prob < 0.05 && this.sprites.length < 10) {
                var hero = new Hero({x_vel: this.getRandomInt(5,8), y_vel: -1*this.getRandomInt(2,6),
                                     flag_x: this.flag.x, flag_y: this.flag.y });
                this.sprites.push(hero);
            }
        },

        draw: function () {
            jaws.context.clearRect(0, 0, jaws.width, jaws.height);
            if (this.gameStart === false) {
                this._drawIntroText();
            } else {
                this._drawScore();
            }

            _.each(this.sprites, function (sprite, i) {
                sprite.draw();
            });
        },

        _drawIntroText: function () {
            jaws.context.font = "bold 60pt terminal";
            jaws.context.lineWidth = 10;
            jaws.context.fillStyle = "black";
            jaws.context.strokeStyle =  "rgba(200,200,200,0.0)";
            jaws.context.fillText("For Victory!", 110, 130);

            jaws.context.font = "bold 20pt terminal";
            jaws.context.fillText("How many times can you win?", 125, 190);

            jaws.context.font = "bold 15pt terminal";
            jaws.context.fillText("Press arrow key to control the flag", 145, 240);
        },

        _drawScore: function () {
            jaws.context.font = "bold 90pt terminal";
            jaws.context.lineWidth = 10;
            jaws.context.fillStyle = "grey";
            jaws.context.strokeStyle =  "rgba(200,200,200,0.0)";
            var scoreStr;
            if (this.score < 10) {
                scoreStr = "00" + parseInt(this.score,10);
            } else if (this.score < 100) {
                scoreStr = "0" + parseInt(this.score, 10);
            } else if (this.score > 999) {
                scoreStr = "999";
            } else {
                scoreStr = parseInt(this.score, 10);
            }
            jaws.context.fillText(scoreStr, 210, 130);
        },

        getRandomInt: function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    };

    return introState;
});