define(['jaws', 'pubsub'], function (jaws, PubSub) {
    var Hero = function (options) {
        var hero = this;

        this.gravity = 0.2;
        this.x_vel = options.x_vel || 0;
        this.y_vel = options.y_vel || 0;
        this.flag_y = options.flag_y || 292;
        this.flag_x = options.flag_x || 400;

        options = _.omit(options, 'x_vel', 'y_vel', 'flag_x', 'flag_y');
        options = _.assign(options, {image: "images/hero.png", x: 0, y: 250});

        this.state = "free";
        this.flag_width = 20;
        this.flag_height = 250;

        jaws.Sprite.call(this, options);

        PubSub.subscribe('flagMove', function (msg, data) {
            hero.flag_x = data[0];
            hero.flag_y = data[1];
        });

        this.update = function () {
            if (this.x > this.flag_x && this.x < this.flag_x + this.flag_width &&
                this.y > this.flag_y && this.y < this.flag_y + this.flag_height) {
                this.moveTo(this.flag_x, this.y + 5);
                if (this.state === "free") {
                    PubSub.publish("score");
                }
                this.state = "attached";

            } else if (this.state === "attached") {
                this.moveTo(this.flag_x, this.y + 5);
            } else {
                this.moveTo(this.x + Math.floor(this.x_vel), this.y + Math.floor(this.y_vel));
                this.y_vel += this.gravity;
            }
        };
    };
    Hero.prototype = jaws.Sprite.prototype;

    return Hero;
});